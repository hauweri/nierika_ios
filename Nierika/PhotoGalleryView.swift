//
//  PhotoGalleryViewTableViewController.swift
//  Nierika
//
//  Created by Juan Carlos on 7/31/18.
//  Copyright © 2018 Juan Carlos. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import ViewAnimator

class PhotoGalleryView: PTTableViewController {
    let domainBase = "https://nierika.cucea.udg.mx/";
    let defaults = UserDefaults.standard
    var arrayPhotos =  [Photograph]()
    var siteName:String?
    var backButton: UIButton?
    var currentIndex = 0
    
    private let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    private let animations = [AnimationType.from(direction: .right, offset: 460.0), AnimationType.zoom(scale: 0.2)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backButton = createBackButton()
        _ = createNavigationBarBackItem(button: backButton)
        showBackButtonDuration(duration: 0.3)
        
        setupActivityIndicator()        
        self.view.backgroundColor = Color.black
        
       
        downloadPhotos()
        
    }
    
    @IBAction func backSiteClick(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func downloadPhotos()
    {
        Alamofire.request(domainBase+"api/photos/", parameters: nil).responseJSON { response in
            switch response.result
            {
            case .success:
                if let data = response.data {
                    self.defaults.set(data, forKey: "photos")
                    self.loadPhotos(photos: data)
                }
            case .failure:
                let alerta = UIAlertController(title: "Error", message: "No es posible descargar la información, revisa tu conexión a internet" , preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation {self.present(alerta, animated: true, completion: nil) }
            }
        }
    }
    		
    private func setupActivityIndicator() {
        activityIndicator.center = CGPoint(x: view.center.x, y: 100.0)
        activityIndicator.hidesWhenStopped = true
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }

    
    
    func loadPhotos(photos: Data)
    {
        do {
            let json  = try JSONSerialization.jsonObject(with: photos, options: .allowFragments)
            if let photos = json as? [[String: Any]]{
                arrayPhotos = photos.compactMap{
                    Photograph(dict: $0)
                }
            }
        }
        catch{
            print("error")
        }
        //print(siteName)
        //let tempArray = arrayPhotos.filter{$0.album != siteName}
        
        //arrayPhotos = tempArray
        
        arrayPhotos.removeAll{
            $0.album != siteName
        }
        
        activityIndicator.stopAnimating()
        self.tableView.reloadData()
        UIView.animate(views: tableView.visibleCells, animations: animations, duration:0.5, completion: {
            
        })
    }
}


extension PhotoGalleryView {
    
    public override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return arrayPhotos.count
    }
    
    public override func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? ParallaxCell else { return }
        
        let index = indexPath.row % arrayPhotos.count
        let imageUrl = arrayPhotos[index].media
        let title = arrayPhotos[index].name
        let url = URL(string: imageUrl)
        
        KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
            cell.setImage(image!, title: title)
        })

    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(ParallaxCell.self, forCellReuseIdentifier: "cell")
        tableView.rowHeight = CGFloat(240)
        let cell: ParallaxCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ParallaxCell
        return cell
    }
    
    public override func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.storyboard(storyboard: .Main)
        let detailView: DetailPhotoView = storyboard.instantiateViewController()
         let index = indexPath.row % arrayPhotos.count
        detailView.photo = arrayPhotos[index]
        detailView.photos = arrayPhotos
        detailView.currentIndex = index
        pushViewController(detailView)
    }
}

extension PhotoGalleryView{
    fileprivate func createBackButton() -> UIButton {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 22, height: 44))
        button.setImage(UIImage.Asset.Back.image, for: .normal)
        button.addTarget(self, action: #selector(PhotoGalleryView.backButtonHandler), for: .touchUpInside)
        return button
    }
    fileprivate func createNavigationBarBackItem(button: UIButton?) -> UIBarButtonItem? {
        guard let button = button else {
            return nil
        }
        
        let buttonItem = UIBarButtonItem(customView: button)
        navigationItem.leftBarButtonItem = buttonItem
        return buttonItem
    }
    
    fileprivate func showBackButtonDuration(duration: Double) {
        backButton?.rotateDuration(duration: duration, from: -CGFloat.pi / 4, to: 0)
        backButton?.scaleDuration(duration: duration, from: 0.5, to: 1)
        backButton?.opacityDuration(duration: duration, from: 0, to: 1)
    }
    
    @objc func backButtonHandler() {
        print("back buttonhandler")
        self.dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "photo_to_site_segue" {
            let controller = segue.destination as? SiteView
            controller?.siteName = self.siteName
        }
    }
}
