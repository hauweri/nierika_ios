//
//  SiteView.swift
//  Nierika
//
//  Created by Juan Carlos on 7/28/18.
//  Copyright © 2018 Juan Carlos. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import FLAnimatedImage
import AMPopTip
import PopupDialog
import LanguageManger_iOS
import AudioPlayer
import ESTMusicIndicator
import Branch
import AVKit


class SiteView: UIViewController, LanguajeProtocol, UIPopoverPresentationControllerDelegate{
    
    let domainBase = "https://nierika.cucea.udg.mx/";
    let defaults = UserDefaults.standard
    var siteName:String?
    var language: String = "español"
    var site: Site?
    var audioPlayer: AudioPlayer?
    
    private var gradient: CAGradientLayer!

    @IBOutlet weak var imageSite: UIImageView!
    @IBOutlet weak var iconSite: UIImageView!
    @IBOutlet weak var nameSite: UILabel!
    @IBOutlet weak var locationSite: UILabel!
    @IBOutlet weak var coordenateSite: UILabel!
    @IBOutlet weak var descriptionSite: UITextView!
    
    @IBOutlet var usedWordsLabel: UILabel!
    
    @IBOutlet weak var descriptionTitle: UILabel!
    //@IBOutlet weak var image_360_preview: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var mediumView: UIView!
    //@IBOutlet weak var imageShadow: UIImageView!
    @IBOutlet weak var keyWordsView: UIView!
    @IBOutlet weak var languageButton: UIBarButtonItem!
    @IBOutlet weak var heightContrain:NSLayoutConstraint!
    
    
    @IBOutlet weak var soundButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    @IBOutlet weak var panoramaButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.bottomView.backgroundColor = UIColor(patternImage: UIImage(named: "textura")!)
        loadSite()
        //configureButtons()
        //imageSite.clipsToBounds = true
    }
    
    func configureButtons(){
        let width: CGFloat = 70
        let height: CGFloat = 70
        let shadowSize: CGFloat = 20
        
        stackView.subviews.forEach{view in
            let contactRect = CGRect(x: -shadowSize, y: height - (shadowSize * 0.4), width: width + shadowSize * 2, height: shadowSize)
            view.layer.shadowPath = UIBezierPath(ovalIn: contactRect).cgPath
            view.layer.shadowRadius = 5
            view.layer.shadowOpacity = 0.4
        }
    }
 
    @IBAction func playSound(_ sender: UIButton) {
        let sound = site?.name.lowercased()
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setActive(true)
           
        } catch {
            print(error)
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        }
        catch {
            // report for an error
        }

        if(audioPlayer == nil){
            do{
                audioPlayer = try AudioPlayer(fileName: sound!+".mp3")
            }catch
            {
                print("no se encuentra el archivo")
            }
            _ = audioPlayer?.duration
            let progressBar = UIProgressView(frame:CGRect(x: 0, y: 0, width: (sender.superview?.frame.width)!,height: 10))
            progressBar.tintColor = UIColor.gray
            progressBar.trackTintColor = UIColor.white
            progressBar.setProgress(0.0, animated: true)
            perform(#selector(updateProgress), with: progressBar, afterDelay: 0.1)
            sender.superview?.addSubview(progressBar)
            
            audioPlayer?.play()
            //self.view.addSubview(indicator)
        }else{
            if(audioPlayer?.isPlaying)!{
                audioPlayer?.stop()
            }else{
                audioPlayer?.play()
            }
        }
    }
    
    @objc func updateProgress(progressBar: UIProgressView){
        progressBar.progress = Float((audioPlayer?.currentTime)!)/Float((audioPlayer?.duration)!)
        
        if Float((audioPlayer?.currentTime)!) < Float((audioPlayer?.duration)!){
            perform(#selector(updateProgress), with: progressBar, afterDelay: 0.1)
        }else{
            progressBar.setProgress(0, animated: true)
        }
    }
    
    @IBAction func openVideoPlayer(_ sender: Any) {
        /*
        audioPlayer?.fadeOut()
        let domain = "https://nierika.cucea.udg.mx"
        let videoURL = URL(string:domain + (site?.video)!)
        let playerVC = MobilePlayerViewController(contentURL: videoURL!)
        playerVC.title = "Nierika - " + (site?.name)!
        playerVC.activityItems = [videoURL!]
        presentMoviePlayerViewControllerAnimated(playerVC) */
        let vc = VideoViewController()
        vc.url = site?.video
        vc.titleString = site?.name
        //navigationController?.pushViewController(vc, animated: true)
        
        present(vc, animated: true, completion: nil)
        
        //performSegue(withIdentifier: "video_segue", sender: nil)
    }
    
    @IBAction func shareTapped(_ sender: Any) {
        let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "nierika_facebook")    // just for tracking count and analytics
        branchUniversalObject.title = site?.name   // This will appear as the shared content's title
        branchUniversalObject.contentDescription = site?.description_wx   // This will appear as the shared content's description
        branchUniversalObject.imageUrl = site?.image
        branchUniversalObject.addMetadataKey("appName", value: "Nierika")
        
        let linkProperties = BranchLinkProperties()
        
        // generate link
        branchUniversalObject.getShortUrl(with: linkProperties, andCallback: { (shareURL, error) in
            if error == nil {
            
                let vc = UIActivityViewController(activityItems: [shareURL!], applicationActivities: [])
                self.present(vc, animated: true)
                if let popOver = vc.popoverPresentationController {
                }
                
            }else{
                print(error)
            }
        })
    }
    
    
    @IBAction func openPanoramaView(_ sender: Any) {
        performSegue(withIdentifier: "panorama_segue", sender: nil)
    }
    
    
    func setLanguageSelected(language: String) {
        print(language)
        self.language = language
        defaults.set(language, forKey: "language")
        var selectedLanguage:Languages = .es
        
        switch language {
        case "wixarika":
            descriptionSite.text = self.site?.description_wx
            selectedLanguage = .es
            languageButton.title = "Wixarika"
            descriptionTitle.text = "Tita ti H+k+"
            usedWordsLabel.text = "Wixárika niukieya"
            adjustUITextViewHeight(arg: descriptionSite)
            searchWords()
        case "español":
            descriptionSite.text = self.site?.description_es
            selectedLanguage = .es
            languageButton.title = "Español"
            descriptionTitle.text  = "Descripción"
            usedWordsLabel.text = "Palabras nativas"
            adjustUITextViewHeight(arg: descriptionSite)
            searchWords()
        case "english":
            descriptionSite.text = self.site?.description_en
            selectedLanguage = .en
            languageButton.title = "English"
            descriptionTitle.text = "About"
                usedWordsLabel.text = "Native words"
            adjustUITextViewHeight(arg: descriptionSite)
            searchWords()
        case "french":
            descriptionSite.text = self.site?.description_fr
            selectedLanguage = .fr
            languageButton.title = "Français"
            descriptionTitle.text = "Tita ti H+k+"
            usedWordsLabel.text = "Mots natifs"
            adjustUITextViewHeight(arg: descriptionSite)
            searchWords()
        
        default:
            descriptionSite.text = self.site?.description_wx
            selectedLanguage = .es
            languageButton.title = "Wixarika"
            descriptionTitle.text = "Tita ti H+k+"
            usedWordsLabel.text = "Wixárika niukieya"
            adjustUITextViewHeight(arg: descriptionSite)
            searchWords()
        }
        //LanguageManger.shared.setLanguage(language: selectedLanguage)
        
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        audioPlayer?.fadeOut()
        performSegue(withIdentifier: "unwindToMain", sender: nil)
    }
    
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        audioPlayer?.fadeOut()
        let photoView = PhotoGalleryView()
        photoView.siteName = siteName!
        navigationController?.pushViewController(photoView, animated: true)
        //performSegue(withIdentifier: "site_photos", sender: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadSite(){
    
    }
    
    
    func loadSite()
    {
        let lang = Locale.current.languageCode
        print(lang)
        
        let langs:[String:String] = ["en":"english", "es": "español", "fr":"frances"]
        nameSite.text = self.site?.name
        locationSite.text = site?.location
            self.coordenateSite.text = (site?.longitude)! + ", " + (site?.latitude)!
        
        let url = URL(string: ((site?.image)!))
        let containerView = UIView(frame: CGRect(x:0,y:0,width:1024,height:683))
        imageSite.kf.indicatorType = .activity
        imageSite.kf.setImage(with: url, completionHandler: {
            (image, error, cacheType, imageUrl) in
            if((error) != nil){
    
                self.addShadow()
            }
            
        })
        let urlIcon = URL(string: ((site?.icon)!))
        iconSite.kf.setImage(with: urlIcon)
        if let langString = langs[lang!]{
            setLanguageSelected(language: langString)
        }else{
            setLanguageSelected(language: "english")
        }
        
        searchWords()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         loadSite()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    @objc func deviceRotated(){
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            print("Landscape")
            keyWordsView.subviews.forEach({ $0.removeFromSuperview() })
        }
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
            print("Portrait")
           keyWordsView.subviews.forEach({ $0.removeFromSuperview() })
        }
    }
    
    func addShadow(){
        imageSite.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        gradient = CAGradientLayer()
        gradient.frame = imageSite.bounds
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradient.locations = [0.7,1]
        
        UIView.animate(withDuration: 4) {
            self.imageSite.layer.addSublayer(self.gradient)
        }
    }
    
    func searchWords(){
        keyWordsView.layer.cornerRadius = 10
        keyWordsView.subviews.forEach{view in
            if view.tag != 1{
                view.removeFromSuperview()
            }
        }
        
        let fuse = Fuse()
        let words = ["Wirikuta", "Kaka+yeri", "Hikuri", "Pariyatsie", "xukuri", "wimari", "ta tewari", "kuayumarie", "Pariyatsie", "Maxakwaxi"]
        let lineArray = descriptionSite.text.components(separatedBy: " ")
        var x:CGFloat  = 10;
        let labelWords = UILabel(frame: CGRect(x:10,y:10, width: 100, height: 15))
        print(lineArray.count)
        //labelWords.text = "Palabras encontradas:"
        let arrayWords = Array(Set(lineArray))
        print(arrayWords.count)
        labelWords.sizeToFit()
        self.keyWordsView.addSubview(labelWords)
        arrayWords.forEach {word in
            words.forEach({
                let result = fuse.search(word.trimmingCharacters(in: .whitespacesAndNewlines), in: $0)
                if(result?.score != nil && (result?.score)! == Double(0.0)){
                    print(word)
                    let label = UILabel(frame: CGRect(x:x, y:30, width: 100, height:40))
                    label.backgroundColor = UIColor.gray
                    label.textColor = UIColor.white
                    label.clipsToBounds = true
                    label.layer.cornerRadius = 10
                    label.textAlignment = NSTextAlignment.center
                    label.text = word
                    label.isUserInteractionEnabled = true
                    //label.padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
                    label.sizeToFit()
                    label.frame = CGRect(x: label.frame.origin.x, y: label.frame.origin.y, width:label.intrinsicContentSize.width + 10, height: label.intrinsicContentSize.height)
                    self.keyWordsView.addSubview(label)
                    x  += label.intrinsicContentSize.width + 10
                    
                    let tap = UITapGestureRecognizer(target: self, action:#selector(openToolTip(sender:)))
                    label.addGestureRecognizer(tap)
                }
            })
            
        }
        self.keyWordsView.layoutIfNeeded()
        
    }
    func adjustUITextViewHeight(arg: UITextView)
    {
        arg.sizeToFit()
        arg.isScrollEnabled = false
        //print(arg.contentSize.height)
        //print(arg.intrinsicContentSize.height)
    }
    
    @objc func openToolTip(sender:UITapGestureRecognizer) {
        let words:[String: String] = ["Wirikuta": "Lugar sagrado que se encuentra en el altiplano desertico de San Luis Potosí México",
                                      "Kaka+yeri": "Nombre dado a los Dioses que se convierten en elementos de la naturaleza",
                                      "Hikuri": "Cactacea que crece en Wirikuta de uso ritual",
                                      "Pariyatsie" : "Lugar donde nació el sol, muchos relatos lo ubican en la cima del Cerro Quemado, San Luis Potosí",
                                      "xukuri": "Recipiente fabricado de la la corteza de calabaza partida a 1 cuarta parte de la base, se utiliza para hacer ofrendas",
                                      "wimari": "Forma coloquial de nombrar a las ofrendas que lleva una mujer  en su paliacate o xikuri",
                                      "ta tewari": "Nuestro abuelo Fuego",
                                      "kuayumarie": "Nuestro hermano mayor venado",
                                      "Maxakwaxi":"Nuestro hermano mayor cola de venado"]
        
        
        let keyWord:UILabel  = sender.view as! UILabel
        let title = keyWord.text
        let message = words.filter{$0.key.lowercased() == keyWord.text?.lowercased()}.first?.value
        let image = UIImage(named: "jicara")
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message, image: image)
        
        // Create buttons
        
        let buttonClose = CancelButton(title: "Cerrar") {
            print("You canceled the car dialog.")
        }
        
        // Add buttons to dialog
        // Alternatively, you can use popup.addButton(buttonOne)
        // to add a single button
        popup.addButtons([buttonClose])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "site_to_photo_segue" {
            print("abriendo fotos")
            let nav = segue.destination as? UINavigationController
            let controller = nav?.viewControllers.first as! PhotoGalleryView
            print(controller)
            controller.siteName = siteName
        }
        if segue.identifier == "panorama_segue" {
            print("abriendo fotos")
            let nav = segue.destination as? UINavigationController
            print(nav?.viewControllers.description)
            let controller = nav?.viewControllers.first as! PanoramaView
            print(controller)
            controller.imageUrl = site?.panorama_image
        }
        
        if segue.identifier == "video_segue"{
            print("abirendo video")
            let controller = segue.destination as? VideoViewController
      
            controller!.url = site?.video
            controller!.titleString = site?.name

        }
        
    }
    
    
    @IBAction func openLanguajeChoser(_ sender: Any) {

        let modal = self.storyboard?.instantiateViewController(withIdentifier: "LenguajeChoseView") as! LenguajeChoseView
        modal.languageProtocol = self
        //modal.modalPresentationStyle  = .popover
        present(modal, animated: true)
    }

}

extension SiteView{
    
}
