//
//  Site.swift
//  Nierika
//
//  Created by Juan Carlos on 7/30/18.
//  Copyright © 2018 Juan Carlos. All rights reserved.
//

import Foundation

class Site{
    var id: Int
    var name: String
    var image:  String
    var icon: String
    var location: String
    var latitude: String
    var longitude: String
    var description_wx: String
    var description_es: String
    var description_en: String
    var description_fr: String
    var panorama_image: String
    var video: String
    
    init?(dict: [String: Any]) {
        
        guard
            let id = dict["id"] as? Int,
            let name = dict["name"] as? String,
            let image = dict["image"] as? String,
            let icon = dict["icon"] as? String,
            let location = dict["location"] as? String,
            let latitude = dict["latitude"] as? String,
            let longitude = dict["longitude"] as? String,
            let description_wx = dict["description_wx"] as? String,
            let description_es = dict["description_es"] as? String,
            let description_en = dict["description_en"] as? String,
            let description_fr = dict["description_fr"] as? String,
            let panorama_image = nullToNil(value: dict["panorama_image"] as? AnyObject) as? String,
            let video = nullToNil(value: dict["video"] as AnyObject) as? String
        else {
            return nil
        }
            
        self.id = id
        self.name = name
        self.image = image
        self.icon = icon
        self.location  = location
        self.latitude = latitude
        self.longitude = longitude
        self.description_wx = description_wx
        self.description_es = description_es
        self.description_en = description_en
        self.description_fr = description_fr
        self.panorama_image = panorama_image
        self.video = video
    }
}
