//
//  Photo.swift
//  Nierika
//
//  Created by Juan Carlos on 7/31/18.
//  Copyright © 2018 Juan Carlos. All rights reserved.
//

import Foundation

class Photograph{
    var id:Int
    var media:String
    var name:String
    var author:String
    var date:String
    var description_wx:String
    var description_es:String
    var description_en:String
    var description_fr:String
    var album: String
    
    init?(dict: [String: Any]) {
        guard
            let id = dict["id"] as? Int,
            let name = dict["name"] as? String,
            let media = dict["media"] as? String,
            let author = dict["author"] as? String,
            let date = dict["date"] as? String,
            let description_wx = dict["description_wx"] as? String,
            let description_es = dict["description_es"] as? String,
            let description_en = dict["description_en"] as? String,
            let description_fr = dict["description_fr"] as? String,
            let album = dict["album"] as? String
        else {
            return nil
        }
        self.id = id
        self.media = media
        self.name = name
        self.author = author
        self.date = date
        self.description_wx = description_wx
        self.description_es = description_es
        self.description_en = description_en
        self.description_fr = description_fr
        self.album = album
    }
}
