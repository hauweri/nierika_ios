//
//  Functions.swift
//  Nierika
//
//  Created by Jose Manuel Chairez Macias on 08/09/18.
//  Copyright © 2018 Juan Carlos. All rights reserved.
//

import Foundation

func nullToNil(value : AnyObject?) -> String? {
    if value is NSNull {
        return ""
    } else {
        return (value as! String)
    }
}

