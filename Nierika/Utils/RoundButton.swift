//
//  RoundButton.swift
//  Nierika
//
//  Created by Juan Carlos on 7/30/18.
//  Copyright © 2018 Juan Carlos. All rights reserved.
//

import UIKit

@IBDesignable
class RoundButton: UIButton {

    @IBInspectable var cornerRadius:CGFloat  = 0{
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWith:CGFloat = 0 {
        didSet{
            self.layer.borderWidth = borderWith
        }
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
}
