//
//  DetailPhotoView.swift
//  Nierika
//
//  Created by Juan Carlos on 7/31/18.
//  Copyright © 2018 Juan Carlos. All rights reserved.
//

import UIKit
import Social
import Alamofire
import Branch


class DetailPhotoView: PTDetailViewController{


    
    @IBOutlet var controlBottomConstrant: NSLayoutConstraint!
    @IBOutlet var controlsViewContainer: UIView!
    @IBOutlet var controlView: UIView!
    @IBOutlet var plusImageView: UIImageView!
    @IBOutlet weak var controlTextLabel: UILabel!
    @IBOutlet var controlTextLableLending: NSLayoutConstraint!
    @IBOutlet var shareImageView: UIButton!
    @IBOutlet var hertIconView: UIButton!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var infoButton: UIButton!
    
    var collapsed :Bool = true
    var controlsHidden: Bool = false
    var backButton: UIButton?
    var photo:Photograph?
    var photos:[Photograph]?
    var domainBase:String = "https://nierika.cucea.udg.mx/"
    var defaults = UserDefaults.standard
    var languageCode:String?
    var language: String?
    var voted:Bool = false
    var currentIndex = 0
    
    var bottomSafeArea: CGFloat {
        var result: CGFloat = 0
        if #available(iOS 11.0, *) {
            result = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        }
        return result
    }
}

extension DetailPhotoView{
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        //let imageTapped  = UITapGestureRecognizer(target: self, action: #selector(DetailPhotoView.imageTapped))
        self.view.subviews[0].isUserInteractionEnabled = true
        let imageSwipe = UISwipeGestureRecognizer(target: self, action: #selector(DetailPhotoView.changePhoto(_:)))
        self.view.subviews[0].addGestureRecognizer(imageSwipe)
        let controlSwipe = UISwipeGestureRecognizer(target:self, action: #selector(DetailPhotoView.toggleDescription(_:)))
        controlSwipe.direction = UISwipeGestureRecognizerDirection.up
        let controlSwipeDown = UISwipeGestureRecognizer(target:self, action: #selector(DetailPhotoView.toggleDescription(_:)))
        controlSwipeDown.direction = UISwipeGestureRecognizerDirection.down
        
        self.controlView.isUserInteractionEnabled = true
        self.controlView.addGestureRecognizer(controlSwipe)
        self.controlView.addGestureRecognizer(controlSwipeDown)
        
        
        controlTextLabel.isUserInteractionEnabled = true
        let authorTapped = UITapGestureRecognizer(target: self, action: #selector(DetailPhotoView.authorTapped(_:)))
        controlTextLabel.addGestureRecognizer(authorTapped)
        
        self.languageCode = defaults.string(forKey: "language")
        backButton = createBackButton()
        _ = createNavigationBarBackItem(button: backButton)
        
        // animations
        showBackButtonDuration(duration: 0.3)
        showControlViewDuration(duration: 0.3)
        //_ = createBlurView()
        infoButton.addTarget(self, action: #selector(DetailPhotoView.plusTapped), for: .touchUpInside)
        
        
        let shareGesture = UITapGestureRecognizer(target: self, action: #selector(DetailPhotoView.shareTapped))
        shareImageView.addGestureRecognizer(shareGesture)
        
        let likeGesture = UITapGestureRecognizer(target: self, action: #selector(DetailPhotoView.likeTapped))
        hertIconView.addGestureRecognizer(likeGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //toggle()
    }
    
   
    
}


extension DetailPhotoView{
    fileprivate func createBackButton() -> UIButton {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 22, height: 44))
        button.setImage(UIImage.Asset.Back.image, for: .normal)
        button.addTarget(self, action: #selector(DetailPhotoView.backButtonHandler), for: .touchUpInside)
        return button
    }
    
    fileprivate func createNavigationBarBackItem(button: UIButton?) -> UIBarButtonItem? {
        guard let button = button else {
            return nil
        }
        
        let buttonItem = UIBarButtonItem(customView: button)
        navigationItem.leftBarButtonItem = buttonItem
        return buttonItem
    }
    
    fileprivate func createBlurView() -> UIView {
        let height = controlView.bounds.height + bottomSafeArea
        let imageFrame = CGRect(x: 0, y: view.frame.size.height - height, width: view.frame.width, height: height)
        let image = view.makeScreenShotFromFrame(frame: imageFrame)
        let screnShotImageView = UIImageView(image: image)
        screnShotImageView.blurViewValue(value: 5)
        screnShotImageView.frame = controlsViewContainer.bounds
        screnShotImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        controlsViewContainer.insertSubview(screnShotImageView, at: 0)
        addOverlay(toView: screnShotImageView)
        return screnShotImageView
    }
    
    fileprivate func addOverlay(toView view: UIView) {
        let overlayView = UIView(frame: view.bounds)
        overlayView.backgroundColor = .black
        overlayView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        overlayView.alpha = 0.4
        view.addSubview(overlayView)
    }
}

extension DetailPhotoView{
    fileprivate func showBackButtonDuration(duration: Double) {
        backButton?.rotateDuration(duration: duration, from: -CGFloat.pi / 4, to: 0)
        backButton?.scaleDuration(duration: duration, from: 0.5, to: 1)
        backButton?.opacityDuration(duration: duration, from: 0, to: 1)
    }
    
    fileprivate func showControlViewDuration(duration: Double) {
        moveUpControllerDuration(duration: duration)
        showControlButtonsDuration(duration: duration)
        showControlLabelDuration(duration: duration, index: currentIndex)
    }
    
    fileprivate func moveUpControllerDuration(duration: Double) {
        
        
        controlBottomConstrant.constant = -controlsViewContainer.bounds.height
        view.layoutIfNeeded()
        
        controlBottomConstrant.constant = 0
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    fileprivate func showControlButtonsDuration(duration: Double) {
       /*
        [plusImageView, shareImageView, hertIconView].forEach {
            $0?.rotateDuration(duration: duration, from: CGFloat.pi / 4, to: 0, delay: duration)
            $0?.scaleDuration(duration: duration, from: 0.5, to: 1, delay: duration)
            $0?.alpha = 1
            $0?.opacityDuration(duration: duration, from: 0, to: 1, delay: duration, remove: false)
        } */
    }
    
    func getDescriptionText(index: Int)->String{
        var description: String = ""
        switch self.languageCode {
        case "español":
             description = photos![index].description_es
        case "english":
            description = photos![index].description_en
        case "wixarika":
            description = photos![index].description_wx
        case "french":
            description = photos![index].description_fr
        default:
            description = photos![index].description_wx
        }
        
        return description
    }
    
    fileprivate func showControlLabelDuration(duration: Double, index: Int) {
        
        controlTextLabel.text = (photos![index].author) + " " + (photos![index].date)
        descriptionText.text = getDescriptionText(index: index)
        self.title = photos![index].name
        if let imageView = self.view.subviews[0] as? UIImageView {
            let url = URL(string:photos![index].media)
            imageView.kf.setImage(with: url)
        }
        descriptionText.textAlignment = NSTextAlignment.justified
        descriptionText.translatesAutoresizingMaskIntoConstraints = true
        descriptionText.sizeToFit()
        descriptionText.isScrollEnabled = true
        descriptionText.textContainerInset = UIEdgeInsetsMake(0, 10, 10, 10)
        controlTextLabel.alpha = 0
        controlTextLabel.opacityDuration(duration: duration, from: 0, to: 1, delay: duration, remove: false)
        
        // move rigth
        let offSet: CGFloat = 20
        controlTextLableLending.constant -= offSet
        view.layoutIfNeeded()
        
        controlTextLableLending.constant += offSet
        UIView.animate(withDuration: duration * 2, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}

extension DetailPhotoView{
    @objc func backButtonHandler() {
        popViewController()
    }
    @objc func plusTapped()
    {
        toggle()
    }
    @objc func changePhoto( _ recognizer : UISwipeGestureRecognizer){
        if recognizer.direction == .right{
            print("Right Swiped")
            if let imageView = self.view.subviews[0] as? UIImageView {
                var index = currentIndex + 1
                if index == (photos?.count)! - 1 {
                    index = -1
                }
                showControlLabelDuration(duration: 0.1, index: currentIndex + 1)
                currentIndex = index
            }
        } else if recognizer.direction == .left {
            print("Left Swiped")
            if let imageView = self.view.subviews[0] as? UIImageView {
                var index = currentIndex - 1
                if index  == -1{
                    index = (photos?.count)!
                }
                let url = URL(string:photos![index - 1].media)
                imageView.kf.setImage(with: url)
            }
        }
        
    }
    
    @objc func toggleDescription(_ recognizer: UISwipeGestureRecognizer){
        
           toggle()
        
    }
    
    func toggle(){
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        let height:CGFloat = screenHeight / 3
        let dheight:CGFloat = descriptionText.bounds.height + 120
        let width:CGFloat = screenWidth
        
        
        if(!collapsed){
            UIView.animate(withDuration: 0.5, animations:{
                print(self.bottomSafeArea)
                self.controlsViewContainer.frame = CGRect(x:0, y: screenHeight - 70 - self.bottomSafeArea, width:width, height:70)
                self.descriptionText.isHidden = true
                self.infoButton.transform = .identity
                self.collapsed = true
            })
        }else{
            UIView.animate(withDuration: 0.5, animations:{
                self.controlsViewContainer.frame = CGRect(x:0, y: screenHeight - dheight, width:width, height:dheight)
                self.descriptionText.isHidden = false
                self.infoButton.transform = CGAffineTransform(rotationAngle: 200)
                self.collapsed = false
                
            })
        }
    }
    
    @objc func shareTapped(){

        let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "nierika_facebook")    //
        branchUniversalObject.title = photos![currentIndex].name   // This will appear as the shared content's title
        branchUniversalObject.contentDescription = photos![currentIndex].description_wx   // This will appear as the shared content's description
        branchUniversalObject.imageUrl = photos![currentIndex].media
        branchUniversalObject.addMetadataKey("appName", value: "Nierika")
        
        let linkProperties = BranchLinkProperties()
        
        // generate link
        branchUniversalObject.getShortUrl(with: linkProperties, andCallback: { (shareURL, error) in
            if error == nil {
                let vc = UIActivityViewController(activityItems: [shareURL!], applicationActivities: [])
                self.present(vc, animated: true)
                if let popOver = vc.popoverPresentationController {
                    popOver.sourceView = self.view
                }
            }else{
                print(error)
            }
        })
        
    }
    
    @objc func authorTapped(_ sender:UITapGestureRecognizer){
        print("opening url")
        if let url = URL(string: domainBase + "photographer/" + controlTextLabel.text!) {
            print("opening url")
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @objc func imageTapped(){
        if(!controlsHidden){
            UIView.animate(withDuration: 2, animations: {
                self.controlsViewContainer.isHidden = true
                self.backButton?.isHidden = true
            })
            controlsHidden = true
        }else{
            UIView.animate(withDuration: 2, animations: {
                self.controlsViewContainer.isHidden = false
                self.backButton?.isHidden = false
            })
            controlsHidden = false
        }
        
    }
    @objc func likeTapped(sender: UIButton){
        print("aca")
        let liked:UIImage = UIImage(named: "liked_icon")!
        
        let parameters: Parameters = [
            "photo": photo?.id ?? 0,
            "rate": 10
        ]
        
        Alamofire.request(domainBase + "api/photos/like/", method: .post, parameters: parameters).responseJSON{ response in
                print(response.result)
                switch response.result
                {
                case .success:
                    UIView.animate(withDuration: 0.6,animations: {
                        self.hertIconView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                                    
                    },completion: { _ in
                        UIView.animate(withDuration: 0.6) {
                            self.hertIconView.transform = CGAffineTransform.identity
                        }
                        self.hertIconView.setImage(liked, for: .normal)
                    })
                case .failure:
                    let alerta = UIAlertController(title: "Error", message: "No es posible registrar tu voto, revisa tu conexión a internet" , preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    OperationQueue.main.addOperation {self.present(alerta, animated: true, completion: nil) }
                }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewShareSegue" {
            //let controller = segue.destination as? ShareView
            //controller?.image = bgImage
        }
    }
}
