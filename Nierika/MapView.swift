//
//  MapView.swift
//  Nierika
//
//  Created by Juan Carlos on 8/5/18.
//  Copyright © 2018 Juan Carlos. All rights reserved.
//

import UIKit
import Mapbox
import Alamofire
import Kingfisher
import PopupDialog
import Turf

class MapView: UIViewController, MGLMapViewDelegate{
    var markerSelected:String?
    var center = CLLocationCoordinate2D(latitude: 23.179895, longitude: -102.871959)
    var centerWixarika = CLLocationCoordinate2D(latitude: 22.158212, longitude: -104.132544)
    var mapView:MGLMapView!
    var progressView: UIProgressView!
    var newData:Bool = false;
    let domain: String = "https://nierika.cucea.udg.mx/api/"
    var defaults = UserDefaults.standard
    var clickCount = 0
    var sites =  [Site]()
    var photos = [Photograph]()
    
    var timer: Timer?
    var polylineSource: MGLShapeSource?
    var currentIndex = 1
    var allCoordinates: [CLLocationCoordinate2D]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let styleURL = URL(string: "mapbox://styles/jcarlos456/cizxkq02p003n2so22qc5yhsm")
        mapView = MGLMapView(frame: view.bounds, styleURL: styleURL)
        mapView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView?.delegate = self
        mapView?.logoView.isHidden = true
        mapView?.attributionButton.isHidden = true
        view.addSubview((mapView)!)
        mapView.setCenter(center, animated: true)
        
        if let cacheSites = defaults.data(forKey: "sites"){
            print("cache disponible, usando datos anteriores")
            loadSites(data: cacheSites)
        }else{
            print("cache no disponible, descargando datos")
            downloadSites()
        }
        
        addButtons()
        
        defaults.set("es", forKey: "language")
        
        // Setup offline pack notification handlers.
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackProgressDidChange), name: NSNotification.Name.MGLOfflinePackProgressChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackDidReceiveError), name: NSNotification.Name.MGLOfflinePackError, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackDidReceiveMaximumAllowedMapboxTiles), name: NSNotification.Name.MGLOfflinePackMaximumMapboxTilesReached, object: nil)

    }
    
    func checkUpdate(){
        Alamofire.request(domain + "api/updates", parameters: nil).responseJSON { response in
            //print(response)
            switch response.result
            {
            case .success:
                if response.data != nil {
                    self.newData = true
                }
            case .failure:
                let alerta = UIAlertController(title: "Error", message: "No es posible verificar actualizaciones, revisa tu conexión a internet" , preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                OperationQueue.main.addOperation {self.present(alerta, animated: true, completion: nil) }
            }
        }
    }
    
    func downloadSites(){
        let url = domain + "sites/"
        Alamofire.request(url).responseJSON{response in
            switch response.result{
                case .success:
                    if let data = response.data {
                        self.defaults.set(data, forKey: "sites")
                        self.loadSites(data: data)
                    }
                case .failure:
                    print("error de conexión")
                }
        }
    }
    
    func loadSites(data: Data){
        let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
        if let sitesArray = json as? [[String: Any]]{
            sites = sitesArray.compactMap{
                Site(dict: $0)
            }
            mapView?.setCenter(center, zoomLevel: 2, direction: 0, animated: false)
            //addMarks()
        }
    }
    
    func updateData(){
        let urls = [domain+"photos", domain + "sites/Wirikuta", domain + "sites/Xapawiyameta", domain + "sites/Haramara"]
        var errors:[String]?
        urls.forEach({ url in
            Alamofire.request(url, parameters: nil).responseJSON { response in
                //	print(response)
                switch response.result
                {
                    case .success:
                        if let data = response.data {
                            self.defaults.set(data, forKey: url)
                            //self.downloadPhotos(data: data)
                        }
                    case .failure:
                        print("error de conexión")
                        errors?.append(url)
                }
            }
        })
    }
    
    func downloadPhotos(){
        let url = domain + "photos/"
        Alamofire.request(url).responseJSON{response in
            switch response.result{
            case .success:
                if let data = response.data {
                    self.defaults.set(data, forKey: "photos")
                    self.decodeData(data: data)
                }
            case .failure:
                print("error de conexión")
            }
        }
    }
    
    func decodeData(data: Data){
        let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
        if let photosArray = json as? [[String: Any]]{
            photos = photosArray.compactMap{
                Photograph(dict: $0)
            }
            dump(photos.count)
        }
        for photo in photos{
            downloadImage(url:photo.media)
        }
        for site in sites{
            downloadImage(url: site.image)
            downloadImage(url: site.panorama_image)
        }
        
    }
    func downloadImage(url:String){
        if(url != ""){
            KingfisherManager.shared.retrieveImage(with: URL(string:url)!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                print("descargando imagen" + url)
            })
        }
    }
    
    func addButtons(){
        let height = view.frame.height
        let width = view.frame.width
        let x = width - 50
        let y = height - height * 0.10
        let yl = height - height * 0.93
        
        let orientation = UIImage(named: "logo_ca")
        let buttonOrientation = UIButton(frame: CGRect(x:(width)-50, y: yl + 40, width:40, height:40))
        buttonOrientation.backgroundColor = UIColor.white
        buttonOrientation.layer.cornerRadius = 20
        buttonOrientation.setImage(orientation, for: .normal)
        buttonOrientation.addTarget(self, action: #selector(MapView.buttoOrientationTapped), for: .touchUpInside)
        
        let image = UIImage(named: "nierika_icon")
        let buttonTour = UIButton(frame: CGRect(x:x, y:y, width: 40, height: 40))
        buttonTour.backgroundColor = UIColor.white
        buttonTour.layer.cornerRadius = 20
        buttonTour.setImage(image, for: .normal)
        buttonTour.addTarget(self, action: #selector(MapView.buttonTourTapped), for: .touchUpInside)
        
        let infoImage = UIImage(named: "info_icon")
        let infoButton = UIButton(frame: CGRect(x:10, y:y, width: 40, height: 40))
        infoButton.layer.cornerRadius = 20
        infoButton.setImage(infoImage, for: .normal)
        infoButton.addTarget(self, action: #selector(infoTapped), for: .touchUpInside)
        
        let labelTitle = UILabel(frame: CGRect(x:(width/2)-50, y:yl, width: 100, height: 40))
        labelTitle.text = "Nierika"
        labelTitle.textAlignment = .center
        labelTitle.font = UIFont(name: labelTitle.font.fontName, size: 20)
        labelTitle.clipsToBounds = true
        labelTitle.backgroundColor = UIColor.white
        labelTitle.layer.cornerRadius = 20
        
        self.view.addSubview(labelTitle)
        self.view.addSubview(buttonTour)
        self.view.addSubview(infoButton)
        self.view.addSubview(buttonOrientation)
    }
    
    @objc func infoTapped(){
        performSegue(withIdentifier: "about_segue", sender: nil)
    }
    
    func addMarks(){
        for site in sites{
            let point = MGLPointAnnotation()
            point.coordinate = CLLocationCoordinate2D(
                latitude: Double(site.latitude) ?? 0,
                longitude: Double(site.longitude) ?? 0)
            point.title = site.name
            point.subtitle = site.location
            mapView.addAnnotation(point)
        }
    }
    
    @IBAction func unwindToMain(segue:UIStoryboardSegue) {
        markerSelected=nil
        let layer = mapView.style?.layer(withIdentifier: "polyline")
        let source = mapView.style?.source(withIdentifier: "polyline")
        if(layer != nil){
            mapView.style?.removeLayer(layer!)
            mapView.style?.removeSource(source!)
        }
        self.mapView.setCenter(self.center, zoomLevel: 5, direction: 0, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        
        let camera = MGLMapCamera(lookingAtCenter: mapView.centerCoordinate, fromDistance: 1800000, pitch: 30, heading: 360)
        mapView.setCamera(camera, withDuration: 5, animationTimingFunction: CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut))
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(6), execute: {
            self.addMarks()
            KingfisherManager.shared.cache.calculateDiskCacheSize(completion: { res in
                if res < 1000{
                     self.aproveDownloadMap()
                }
            })
        })
        
        
    }
    
    func aproveDownloadMap()
    {
        let popup = PopupDialog(title: "Descargar datos", message: "Tienes la posibilidad de contar con los datos de esta aplicación en modo OFFLINE, para esto es necesario descargar los datos de internet, asegúrate de estar conectado por Wifi")
        let buttonClose = CancelButton(title: "Cerrar") {
            
        }
        let buttonAccept = DefaultButton(title: "Descargar ahora"){
            //self.startOfflinePackDownload()
            self.downloadPhotos()
        }
        popup.addButtons([buttonClose,buttonAccept])
        self.present(popup, animated: true, completion: nil)
    }
    
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        if let title:String = annotation.title!{
            markerSelected = title
        }else{
            print("error")
            print(markerSelected)
        }
        
        //
        switch annotation.title {
            case "Wirikuta":
                allCoordinates = self.wirikutaCoordinates()
            case "Xapawiyameta":
                allCoordinates = self.xapawiyemetaCoordinates()
            case "Haramara":
                allCoordinates = self.haramaraCoordinates()
            case "Hauxamanakaa":
                allCoordinates = self.hauxamanakaCoordinates()
            case "Teekata":
                 self.openSite()
                 return  true
            case .none:
                return true
            case .some(_):
                return true
            
        }
        
        addPolyline(to: mapView.style!)
        animatePolyline()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(0), execute: {
            //self.openSite()
            let camera = MGLMapCamera(lookingAtCenter: annotation.coordinate, fromDistance: 550000, pitch: 30, heading: 180)
            mapView.setCamera(camera, withDuration: 5, animationTimingFunction: CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear))
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
                self.openSite()
            })
        })
        return true
    }
    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        guard annotation is MGLPointAnnotation else {
            return nil
        }
        let reuseIdentifier = "\(String(describing: annotation.title))"
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        //print(annotationView?.annotation?.title)
        if annotationView == nil {
            annotationView = SiteAnnotationView(reuseIdentifier: reuseIdentifier)
            annotationView!.bounds = CGRect(x: 0, y: 0, width: 40, height: 40)
            let imageString = (annotation.title??.lowercased() ?? "default")! + "_mark"
            print(imageString)
            let mark  = UIImage(named: imageString)
            let imageView = UIImageView(image: mark!)
            imageView.frame = CGRect(x: 0, y: 0, width: 40, height: 50)
            let animation = CABasicAnimation(keyPath: "borderWidth")
            animation.duration = 0.1
            //annotationView?.animate(animations: [])
            annotationView?.addSubview(imageView)
        }
        
        return annotationView
    }
    
    func openSite(){
        let vc = SiteView()
        if let site = sites.first(where: {$0.name == markerSelected}){
            vc.site = site
            print(site)
            performSegue(withIdentifier: "site", sender: nil)
        }else{
            print("error")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "site" {
            let nav = segue.destination as? UINavigationController
            let controller = nav?.viewControllers.first as! SiteView
            if let site = sites.first(where: {$0.name == markerSelected}){
                controller.site = site
            }
            controller.siteName = markerSelected
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func startOfflinePackDownload() {
        let region = MGLTilePyramidOfflineRegion(styleURL: mapView.styleURL, bounds: mapView.visibleCoordinateBounds, fromZoomLevel: mapView.zoomLevel, toZoomLevel: mapView.zoomLevel + 1)
        
        

        let userInfo = ["name": "packOffline"]
        let context = NSKeyedArchiver.archivedData(withRootObject: userInfo)
        
        
        MGLOfflineStorage.shared.addPack(for: region, withContext: context) { (pack, error) in
            
            guard error == nil else {
                print("Error: \(error?.localizedDescription ?? "unknown error")")
                return
            }
            
            pack!.resume()
            
        }
        
    }
    
    @objc func offlinePackProgressDidChange(notification: NSNotification) {
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String] {
            let progress = pack.progress
            let completedResources = progress.countOfResourcesCompleted
            let expectedResources = progress.countOfResourcesExpected
            let progressPercentage = Float(completedResources) / Float(expectedResources)
            
            if progressView == nil {
                progressView = UIProgressView(progressViewStyle: .default)
                //let frame = view.bounds.size
                //progressView.frame = CGRect(x: frame.width / 4, y: frame.height * 0.75, width: frame.width / 2, height: 10)
                //view.addSubview(progressView)
            }
            print(progressPercentage)
            progressView.progress = progressPercentage
            
            // If this pack has finished, print its size and resource count.
            if completedResources == expectedResources {
                let byteCount = ByteCountFormatter.string(fromByteCount: Int64(pack.progress.countOfBytesCompleted), countStyle: ByteCountFormatter.CountStyle.memory)
                print("Offline map “\(userInfo["name"] ?? "unknown")” completed: \(byteCount), \(completedResources) resources")
                progressView.isHidden = true
            } else {
                // Otherwise, print download/verification progress.
                //print("Offline pack “\(userInfo["name"] ?? "unknown")” has \(completedResources) of \(expectedResources) resources — \(progressPercentage * 100)%.")
                //downloadPhotos()
            }
        }
    }
    
    @objc func offlinePackDidReceiveError(notification: NSNotification) {
        print(notification)
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String],
            let error = notification.userInfo?[MGLOfflinePackUserInfoKey.error] as? NSError {
            print("Offline pack “\(userInfo["name"] ?? "unknown")” received error: \(error.localizedFailureReason ?? "unknown error")")
        }
    }
    
    @objc func offlinePackDidReceiveMaximumAllowedMapboxTiles(notification: NSNotification) {
        print(notification)
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String],
            let maximumCount = (notification.userInfo?[MGLOfflinePackUserInfoKey.maximumCount] as AnyObject).uint64Value {
            print("Offline pack “\(userInfo["name"] ?? "unknown")” reached limit of \(maximumCount) tiles.")
        }
    }
}

extension MapView {
    @objc func buttonTourTapped(){
        self.mapView?.setCenter(center, zoomLevel: 5, direction: 0, animated: false)
    }
    
    @objc func buttoOrientationTapped(){
        self.mapView?.setCenter(centerWixarika, zoomLevel: 5, direction: 62, animated: false)
    }
    
    func addPolyline(to style: MGLStyle) {
        // Add an empty MGLShapeSource, we’ll keep a reference to this and add points to this later.
        let source = MGLShapeSource(identifier: "polyline", shape: nil, options: nil)
        style.addSource(source)
        polylineSource = source
        
        // Add a layer to style our polyline.
        let layer = MGLLineStyleLayer(identifier: "polyline", source: source)
        layer.lineJoin = NSExpression(forConstantValue: "round")
        layer.lineCap = NSExpression(forConstantValue: "round")
        layer.lineColor = NSExpression(forConstantValue: UIColor.red)
        
        // The line width should gradually increase based on the zoom level.
        layer.lineWidth = NSExpression(format: "mgl_interpolate:withCurveType:parameters:stops:($zoomLevel, 'linear', nil, %@)",
                                       [14: 5, 18: 20])
        style.addLayer(layer)
    }
    
    func animatePolyline() {
        currentIndex = 1
        timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(tick), userInfo: nil, repeats: true)
    }
    
    @objc func tick() {
        if currentIndex > allCoordinates.count {
            timer?.invalidate()
            timer = nil
            return
        }
        
        // Create a subarray of locations up to the current index.
        let coordinates = Array(allCoordinates[0..<currentIndex])
        
        // Update our MGLShapeSource with the current locations.
        updatePolylineWithCoordinates(coordinates: coordinates)
        currentIndex += 1
    }
    
    func updatePolylineWithCoordinates(coordinates: [CLLocationCoordinate2D]) {
        var mutableCoordinates = coordinates
        
        let polyline = MGLPolylineFeature(coordinates: &mutableCoordinates, count: UInt(mutableCoordinates.count))
        
        // Updating the MGLShapeSource’s shape will have the map redraw our polyline with the current coordinates.
        polylineSource?.shape = polyline
    }
    
    func wirikutaCoordinates() -> [CLLocationCoordinate2D] {
        return [
            ( -104.204765658563005, 22.256387522459629 ), ( -104.206596118200295, 22.25500698040204 ), ( -104.213106662793606, 22.24578786692981 ), ( -104.228850258351599, 22.246232096475619 ), ( -104.254693538697097, 22.263996610543671 ), ( -104.283223354510596, 22.29341972967746 ), ( -104.288592199266304, 22.3444603994799 ), ( -104.307232864715004, 22.450988315670401 ), ( -104.266716432175997, 22.586638681555812 ), ( -104.257599135717498, 22.73498204014761 ), ( -104.1363553666679, 22.768699144110411 ), ( -103.962843142446403, 22.644513055817988 ), ( -103.898141039450493, 22.625317182644402 ), ( -103.859766154495404, 22.64959297575497 ), ( -103.850562746583094, 22.688486997655321 ), ( -103.735004951669296, 22.6844903249978 ), ( -103.718177580552705, 22.692762031426689 ), ( -103.691439941899006, 22.679337440436498 ), ( -103.585838545150395, 22.711930255668921 ), ( -103.572134359277996, 22.759561045394541 ), ( -103.533025150585701, 22.800360001441351 ), ( -103.438504732881398, 22.82661639044484 ), ( -103.373373609469098, 22.865050461665749 ), ( -103.3395165448418, 22.912211808179201 ), ( -103.305838739919196, 22.911955737338971 ), ( -103.263772253289105, 22.96777770224675 ), ( -103.2305445543806, 22.982707401320219 ), ( -102.991432884485803, 23.178471523574729 ), ( -102.867251632937098, 23.174606119639112 ), ( -102.856013775316598, 23.16719164406901 ), ( -102.627562212896905, 22.864936972782989 ), ( -102.486926075612203, 22.81758260919888 ), ( -102.454630697637995, 22.74987546970311 ), ( -102.329710451020006, 22.721542478193761 ), ( -101.733665145723407, 22.618319840214362 ), ( -101.697327325503394, 22.637390122930569 ), ( -101.705556767263104, 22.780169035785448 ), ( -101.681343749376893, 22.89591489805041 ), ( -101.725366824630797, 23.003962643313489 ), ( -101.784445426271802, 23.093773391462609 ), ( -101.718769402262694, 23.172643593196721 ), ( -101.734565965493701, 23.317034912215441 ), ( -101.517993674899898, 23.345249548614159 ), ( -101.196777467013405, 23.17853839282337 ), ( -101.210439615555302, 23.223809780788951 ), ( -101.211475747372901, 23.281174729661469 ), ( -101.202370254340394, 23.319233646155059 ), ( -101.185831666368202, 23.321391438288121 ), ( -101.183616440384597, 23.325929035470139 ), ( -101.190599923285504, 23.338840422934581 ), ( -101.187905021290305, 23.345886554351789 ), ( -101.177775509593204, 23.350484006347621 ), ( -101.170288656386404, 23.394665878094461 ), ( -101.174503269159999, 23.422266932289389 ), ( -101.173566402001796, 23.445423972337551 ), ( -101.172920585001506, 23.487186861789311 ), ( -101.132192105631205, 23.52301023704204 ), ( -101.091046650916795, 23.545077389868329 ), ( -101.085215429800598, 23.557980073875811 ), ( -101.087820208978101, 23.563264504969279 )
            ].map({CLLocationCoordinate2D(latitude: $0.1, longitude: $0.0)})
    }
    
    func xapawiyemetaCoordinates() -> [CLLocationCoordinate2D] {
        return [
            ( -104.205702288633105, 22.254369774308859 ), ( -104.226536386179504, 22.245442661915209 ), ( -104.2881679712101, 22.291902841538001 ), ( -104.341704746298504, 22.4076029632852 ), ( -104.263319821195694, 22.615847914876859 ), ( -104.070087295073705, 22.681591500255809 ), ( -103.893753499649904, 22.62786453529781 ), ( -103.731607808153996, 22.3898835166063 ), ( -103.5670498009802, 22.301735162604128 ), ( -103.2110332701521, 22.346669670819651 ), ( -103.266875943491002, 22.112311793965219 ), ( -103.301917428414399, 21.777305865969701 ), ( -103.459834021974899, 21.46653067212884 ), ( -103.4682038580127, 21.206087500382079 ), ( -103.4301539695564, 21.046140294433751 ), ( -103.461604195610406, 20.781437288016221 ), ( -103.402294698538199, 20.738867278073769 ), ( -103.425911760557995, 20.613410674391169 ), ( -103.322385455048803, 20.589268541433832 ), ( -103.214774132870204, 20.37431559763057 ), ( -103.189230349020406, 20.341763890169219 ), ( -103.188830305905697, 20.287105622986608 ), ( -103.172093638843506, 20.251809540037812 ) 
            ].map({CLLocationCoordinate2D(latitude: $0.1, longitude: $0.0)})
    }
    
    func hauxamanakaCoordinates() -> [CLLocationCoordinate2D] {
        return [
            ( -104.204830193101301, 22.2562239868825 ), ( -104.213070326853796, 22.245762184245311 ), ( -104.228840389840002, 22.24615932154266 ), ( -104.254775090162198, 22.264203705294872 ), ( -104.2829370556924, 22.293292676008409 ), ( -104.308016900456096, 22.45228476138411 ), ( -104.299387685797797, 22.476487843251 ), ( -104.266605442156106, 22.587228468953889 ), ( -104.263932946413306, 22.61712385074669 ), ( -104.138419058130907, 22.638858940287911 ), ( -104.073131004173902, 22.684552467814591 ), ( -103.898247938765493, 22.625098060256612 ), ( -103.850019547872506, 22.688398595922742 ), ( -103.718296199060404, 22.69220967386266 ), ( -103.690545860318295, 22.67986140483465 ), ( -103.585321346984003, 22.712204891787689 ), ( -103.5330444238936, 22.79970584266745 ), ( -103.4383532474058, 22.827278819249081 ), ( -103.340102204687, 22.91168097666921 ), ( -103.264223483333296, 22.967590886761698 ), ( -102.990975768593401, 23.17803985713088 ), ( -102.868086510079493, 23.1749260904806 ), ( -102.956588407145503, 23.28933375221397 ), ( -102.961539367296098, 23.457433137528259 ), ( -103.121408495002996, 23.499165795042551 ), ( -103.634126687008603, 23.657539287366031 ), ( -103.815922392743801, 23.744901258578281 ), ( -103.985397345744104, 23.740268784820721 ), ( -104.230820167924804, 23.847516365281439 ), ( -104.337306309895794, 23.90224780495954 ), ( -104.391302747025506, 24.014574890181709 ), ( -104.661391293732606, 24.016003570354691 ), ( -104.668942134278794, 23.952137159409791 ), ( -104.703675041611604, 23.904976737006709 ), ( -104.756560940253706, 23.906585615212251 ), ( -104.756357214857005, 23.791691931446159 ), ( -104.727099644018793, 23.702054193739261 ), ( -104.751210647563099, 23.617713381614909 ), ( -104.723016688596999, 23.53203468382516 ), ( -104.914457304828204, 23.480216169195501 ), ( -104.939617420875607, 23.473563580137171 ), ( -104.907991387784094, 23.40631205009846 ), ( -104.8424155139828, 23.365798731282041 ), ( -104.903433906453202, 23.350736579950961 ), ( -104.911116837070594, 23.288582968450839 ), ( -104.918812762040403, 23.227812689324811 ), ( -104.943205789037094, 23.206386010335901 )
            ].map({CLLocationCoordinate2D(latitude: $0.1, longitude: $0.0)})
    }
    func haramaraCoordinates() -> [CLLocationCoordinate2D] {
        return [
            ( -104.207264476500697, 22.254203679159829 ), ( -104.227286547251197, 22.245723460955571 ), ( -104.2860572377853, 22.29300981383507 ), ( -104.343648260563, 22.408062367947299 ), ( -104.356864253586096, 22.349503540806989 ), ( -104.400921294690704, 22.32879155888703 ), ( -104.471011799094597, 22.300552290837409 ), ( -104.515754664344399, 22.251615204555211 ), ( -104.789171572147794, 22.14480865042389 ), ( -105.1404917939889, 21.9567590189247 ), ( -105.208583151668805, 21.805420984429421 ), ( -105.282560028180001, 21.54942122811477 ), ( -105.302298930722102, 21.528893851260531 )
            ].map({CLLocationCoordinate2D(latitude: $0.1, longitude: $0.0)})
    }
    
    
}

class SiteAnnotationView: MGLAnnotationView {
    
    var clickCount = 0
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

