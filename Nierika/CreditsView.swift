//
//  CreditsView.swift
//  Nierika
//
//  Created by hauweri on 12/11/18.
//  Copyright © 2018 Juan Carlos. All rights reserved.
//

import UIKit

class CreditsView: UIViewController, UITextViewDelegate {
    @IBOutlet var logoView: UIImageView!
    @IBOutlet var viewCenter: UIView!
    @IBOutlet weak var textView: UITextView!{
        didSet {
            textView.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
