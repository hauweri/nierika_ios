//
//  VideoViewController.swift
//  Nierika
//
//  Created by Juan Carlos on 3/6/19.
//  Copyright © 2019 Juan Carlos. All rights reserved.
//

import UIKit
import BMPlayer

class VideoViewController: UIViewController{
    fileprivate var player = BMPlayer()
    var url:String?
    var titleString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.player = BMPlayer()
        view.addSubview(player)
        
        player.snp.makeConstraints{(make) in
            make.top.equalTo(self.view).offset(10)
            make.bottom.equalTo(self.view).offset(-20)
            make.left.right.equalTo(self.view)
            make.height.equalTo(player.snp.width).multipliedBy(9.0/16.0).priority(750)
        }
        
        
        
        player.backBlock = {[unowned self] (isFullScreen) in
            if isFullScreen == true {return}
            self.dismiss(animated: true, completion: nil)
        }
        if let urlO = URL(string: url!){
            let asset = BMPlayerResource(url: URL(string: url!)!, name: titleString!)
            player.setVideo(resource: asset)
            player.autoPlay()
        }
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


