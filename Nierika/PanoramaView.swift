//
//  PanoramaView.swift
//  Nierika
//
//  Created by Jose Manuel Chairez Macias on 20/08/18.
//  Copyright © 2018 Juan Carlos. All rights reserved.
//

import Foundation
import MobileCoreServices
import UIKit
import AVFoundation
import Alamofire
import AlamofireImage
import NVActivityIndicatorView

class PanoramaView: UIViewController, NVActivityIndicatorViewable {
    @IBOutlet var imageVRView: GVRPanoramaView!
    
    
    @IBOutlet var loadingView: NVActivityIndicatorView!
    
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func shareTapped(_ sender: Any) {
    }
    //@IBOutlet var videoVRView: GVRVideoView!
    var currentView: UIView?
    var currentSite: String?
    var imageUrl:String?
    var domainBase :String = "https://nierika.cucea.udg.mx"
    var currentDisplayMode = GVRWidgetDisplayMode.embedded
    var isPaused = true
    
    enum Media {
        static var photoArray = ["wirikuta_360_1"]
        static let videoURL = "Produce_6.mp4"
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        view.backgroundColor  = UIColor.black
        imageVRView.delegate = self
        let frame = CGRect(x: 100, y: 200, width: 60, height: 60)
        
        loadingView.type = .ballClipRotate
        loadingView.color = UIColor.white
        loadingView.startAnimating()
        

        downloadImage()
        
        imageVRView.enableCardboardButton = true
        imageVRView.enableFullscreenButton = true
        imageVRView.enableTouchTracking = true
        imageVRView.enableCardboardButton = true
        
        //videoVRView.load(from: URL(string: Media.videoURL))
        //videoVRView.enableFullscreenButton = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadImage(){
        Alamofire.request(imageUrl!).responseImage { response in
            if let image = response.result.value {
                print("image downloaded: \(image)")
                self.imageVRView.load(image)
            }else{
                let alert = UIAlertController(title: "Imagen no disponible", message: "Por el momento no contamos con una imagen para este sitio!.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Gracias", style: .default, handler: { action in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true)
            }
        }
        loadingView.stopAnimating()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PanoramaView: GVRWidgetViewDelegate{
    func widgetView(_ widgetView: GVRWidgetView!, didLoadContent content: Any!) {
        if content is UIImage {
            imageVRView.isHidden = false
           
        }
    }
    
    func widgetView(_ widgetView: GVRWidgetView!, didFailToLoadContent content: Any!, withErrorMessage errorMessage: String!)  {
        print(errorMessage)
    }
    
    func widgetView(_ widgetView: GVRWidgetView!, didChange displayMode: GVRWidgetDisplayMode) {
        currentView = widgetView
        currentDisplayMode = displayMode
        if currentView == imageVRView && currentDisplayMode != GVRWidgetDisplayMode.embedded {
            view.isHidden = true
        } else {
            view.isHidden = false
        }
    }
    
    func widgetViewDidTap(_ widgetView: GVRWidgetView!) {
        guard currentDisplayMode != GVRWidgetDisplayMode.embedded else {return}
        if currentView == imageVRView {
            Media.photoArray.append(Media.photoArray.removeFirst())
            imageVRView?.load(UIImage(named: Media.photoArray.first!), of: GVRPanoramaImageType.mono)
        }
    }
}

extension PanoramaView{
    func getThumbnailFrom(path: URL) -> UIImage? {
        do {
            let asset = AVURLAsset(url: path , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            
            return thumbnail
            
        } catch let error {
            
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
        
    }
}
