//
//  IntroViewController.swift
//  Nierika
//
//  Created by hauweri on 1/31/19.
//  Copyright © 2019 Juan Carlos. All rights reserved.
//

import UIKit
import PopupDialog
import Alamofire
import Kingfisher
import Mapbox

protocol receiveData {
    func pass()
}

class IntroViewController: UIViewController, OnboardPageViewControllerDelegate, receiveData{
    func pageViewController(_ pageVC: OnboardPageViewController, actionTappedAt index: Int) {
        print(index)
    }
    
    func pageViewController(_ pageVC: OnboardPageViewController, advanceTappedAt index: Int) {
        print(index)
    }
    
    func pass() { //conforms to protocol
        print("ya")
       //performSegue(withIdentifier: "main_segue", sender: nil)
    }
    
    let domain: String = "https://nierika.cucea.udg.mx/api/"
    var defaults = UserDefaults.standard
    var sites =  [Site]()
    var photos = [Photograph]()
    var completedResources = 0
    var progressView: UIProgressView!
    var statusText: UILabel!
    var presenting: Bool = true
    var hashV: Int?
    
    let welcome = "welcome_text".localized(withComment: "Seasons greeting")
    
    lazy var onboardingPages: [OnboardPage] = {
        let page1 = OnboardPage(title: NSLocalizedString("welcome_title", comment: ""),
                                imageName: "icon_app",
                                description: NSLocalizedString("welcome_text", comment: ""))
        
        let page2 = OnboardPage(title: NSLocalizedString("maps_title", comment: ""),
                                imageName: "maps_icon",
                                description: NSLocalizedString("maps_text", comment: ""))
        
        let page3 = OnboardPage(title: NSLocalizedString("photos_title", comment: ""),
                                imageName: "offline_icon",
                                description: NSLocalizedString("photos_text", comment: ""),
                                advanceButtonTitle: NSLocalizedString("skip", comment: ""),
                                actionButtonTitle: NSLocalizedString("donwload_button", comment: ""),
                                action: { [weak self] completion in
                                    self?.aproveDownload(completion)
                                })
        return [page1, page2, page3]
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        print(defaults.bool(forKey: "first"))
        
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackProgressDidChange), name: NSNotification.Name.MGLOfflinePackProgressChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackDidReceiveError), name: NSNotification.Name.MGLOfflinePackError, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackDidReceiveMaximumAllowedMapboxTiles), name: NSNotification.Name.MGLOfflinePackMaximumMapboxTilesReached, object: nil)
        if(defaults.bool(forKey: "first")){
            performSegue(withIdentifier: "main_segue", sender: nil)
        }else{
            perform(#selector(presentIntroController), with: nil, afterDelay: 0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear")
        let actionButtonStyling: OnboardViewController.ButtonStyling = { button in
            button.setTitleColor(.black, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 22.0, weight: .semibold)
            button.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
            button.layer.cornerRadius = button.bounds.height / 2.0
            button.contentEdgeInsets = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
            button.layer.shadowColor = UIColor.black.cgColor
            button.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
            button.layer.shadowRadius = 2.0
            button.layer.shadowOpacity = 0.2
        }
        let appearance = OnboardViewController.AppearanceConfiguration(tintColor: .orange,
                                                                       textColor: .white,
                                                                       backgroundColor: .black,
                                                                       titleFont: UIFont.boldSystemFont(ofSize: 32.0),
                                                                       textFont: UIFont.boldSystemFont(ofSize: 17.0), actionButtonStyling: actionButtonStyling)
        
        let onboardingViewController = OnboardViewController(pageItems: onboardingPages, appearanceConfiguration: appearance)
        onboardingViewController.modalPresentationStyle = .formSheet
        onboardingViewController.viewWillAppear(animated)
        onboardingViewController.didMove(toParentViewController: self)
        if (hashV != nil){
            presenting = false
        }else{
            hashV = (onboardingViewController.presentationController?.presentedViewController.hash)!
            presenting = true
        }

        if presenting  == false || defaults.bool(forKey: "first"){
            performSegue(withIdentifier: "main_segue", sender: nil)
        }else{
            onboardingViewController.presentFrom(self, animated: true)
        }
    }
    
    @objc func presentIntroController(){
        
    }

    func aproveDownload(_ completion: @escaping (_ success: Bool, _ error: Error?) -> Void)
    {
        let popup = PopupDialog(title: NSLocalizedString("download_title", comment: "") , message: NSLocalizedString("download_advertise", comment: ""))
        let buttonClose = CancelButton(title: NSLocalizedString("cancel_download", comment: "")) {
            self.defaults.set(true, forKey: "first")
            self.presenting = false
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let startingView = storyboard.instantiateViewController(withIdentifier: "map_view")
            self.presentedViewController!.present(startingView, animated: true)
        }
        let buttonAccept = DefaultButton(title: NSLocalizedString("accept_download", comment: "")){
            self.startOfflinePackDownload()
            //self.downloadPhotos()
        }
        
        popup.addButtons([buttonAccept,buttonClose])
        presentedViewController?.present(popup, animated: true, completion: nil)
    }
    
    func downloadPhotos(){
        let url = domain + "photos/"
        Alamofire.request(url).responseJSON{response in
            switch response.result{
            case .success:
                if let data = response.data {
                    self.defaults.set(data, forKey: "photos")
                    self.decodeData(data: data)
                }
            case .failure:
                print("error de conexión")
            }
        }
        
    }
    
    func decodeData(data: Data){
        if progressView == nil {
            progressView = UIProgressView(progressViewStyle: .default)
            let frame = view.frame.size
            print(frame)
            statusText = UILabel(frame: CGRect(x: frame.width / 4, y: frame.height / 2, width: frame.width - 10, height: 10))
            statusText.textColor = UIColor.white
            statusText.textAlignment = .center
            statusText.font = UIFont.systemFont(ofSize: 10.0)
            progressView.frame = CGRect(x: frame.width / 8, y: frame.height / 2, width: frame.width / 2, height: 10)
            //print(presentationController.)
            let currentView = presentedViewController?.view.subviews[0].subviews[1].subviews[1].subviews[0]
            currentView!.addSubview(progressView)
            currentView!.addSubview(statusText)
        }
        
        progressView.isHidden = false
        progressView.progress = 0.0
        let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
        if let photosArray = json as? [[String: Any]]{
            photos = photosArray.compactMap{
                Photograph(dict: $0)
            }
        }
        self.progressView.progress = 0.0
        self.completedResources = 0
        for photo in photos{
            downloadImage(url:photo.media)
        }
        self.completedResources = 0
        for site in sites{
            downloadImage(url: site.image)
            downloadImage(url: site.panorama_image)
        }
        //self.presentedViewController?.performSegue(withIdentifier: "main_segue", sender: nil)
        self.defaults.set(true, forKey: "first")
        self.presenting = false
        self.dismiss(animated: true, completion: nil)
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let startingView = storyboard.instantiateViewController(withIdentifier: "map_view")
        self.present(startingView, animated: true)
    }
    func downloadImage(url:String){
        if(url != ""){
            KingfisherManager.shared.retrieveImage(with: URL(string:url)!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                print("Descargando fotos")
                self.statusText.text =  NSLocalizedString("download_text", comment: "")
                self.completedResources += 1
                let progressPercentage = (Float(self.completedResources) * 100) / (Float(self.photos.count) * 2)
                print(progressPercentage)
                self.progressView.progress = progressPercentage
            })
        }
    }
    
    func startOfflinePackDownload() {
        let zoomLevel = 5.429193221008468
        let styleURL = URL(string: "mapbox://styles/jcarlos456/cizxkq02p003n2so22qc5yhsm")
        let sw = CLLocationCoordinate2D(latitude: 18.430454639986067, longitude: -106.66087018401703)
        let ne = CLLocationCoordinate2D(latitude: 30.290196707939785, longitude: -99.08304781597542)
        
        let bounds = MGLCoordinateBounds(sw: sw, ne: ne)
        let region = MGLTilePyramidOfflineRegion(styleURL: styleURL, bounds: bounds, fromZoomLevel: zoomLevel, toZoomLevel: zoomLevel + 1)
        
        let userInfo = ["name": "My Offline Pack"]
        let context = NSKeyedArchiver.archivedData(withRootObject: userInfo)
        
        MGLOfflineStorage.shared.addPack(for: region, withContext: context) { (pack, error) in
        guard error == nil else {
                print("Error: \(error?.localizedDescription ?? "unknown error")")
                return
            }
            pack!.resume()
            
        }
        
    }
    
    @objc func offlinePackProgressDidChange(notification: NSNotification) {
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String] {
            let progress = pack.progress
            let completedResources = progress.countOfResourcesCompleted
            let expectedResources = progress.countOfResourcesExpected
            let progressPercentage = Float(completedResources) / Float(expectedResources)
            
            
            if progressView == nil {
                progressView = UIProgressView(progressViewStyle: .default)
                let frame = view.bounds.size
                statusText = UILabel(frame: CGRect(x: 5, y: frame.height / 2 + 12, width: frame.width - 10, height: 10))
                statusText.textColor = UIColor.white
                statusText.textAlignment = .center
                statusText.font = UIFont.systemFont(ofSize: 10.0)
                progressView.frame = CGRect(x: frame.width / 8, y: frame.height / 2, width: frame.width / 2, height: 10)
                //print(presentationController.)
                
                let currentView = presentedViewController?.view.subviews[0].subviews[0].subviews[1].subviews[0]
                //let currentView = presentedViewController?.view.subviews[0].subviews[1].subviews[1].subviews[0]
                print(currentView)
                currentView!.addSubview(progressView)
                currentView!.addSubview(statusText)
            }
            
            progressView.progress = progressPercentage
            
            // If this pack has finished, print its size and resource count.
            if completedResources == expectedResources {
                let byteCount = ByteCountFormatter.string(fromByteCount: Int64(pack.progress.countOfBytesCompleted), countStyle: ByteCountFormatter.CountStyle.memory)
                //print("Offline map “\(userInfo["name"] ?? "unknown")” completed: \(byteCount), \(completedResources) resources")
                progressView.isHidden = true
                downloadPhotos()
            } else {
                // Otherwise, print download/verification progress.
                //print("Offline pack “\(userInfo["name"] ?? "unknown")” has \(completedResources) of \(expectedResources) resources — \(progressPercentage * 100)%.")
                
            }
        }
    }
    
    @objc func offlinePackDidReceiveError(notification: NSNotification) {
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String],
            let error = notification.userInfo?[MGLOfflinePackUserInfoKey.error] as? NSError {
            print("Offline pack “\(userInfo["name"] ?? "unknown")” received error: \(error.localizedFailureReason ?? "unknown error")")
        }
    }
    
    @objc func offlinePackDidReceiveMaximumAllowedMapboxTiles(notification: NSNotification) {
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String],
            let maximumCount = (notification.userInfo?[MGLOfflinePackUserInfoKey.maximumCount] as AnyObject).uint64Value {
            print("Offline pack “\(userInfo["name"] ?? "unknown")” reached limit of \(maximumCount) tiles.")
        }
    }
    
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


