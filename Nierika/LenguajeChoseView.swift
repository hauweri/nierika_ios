//
//  LenguajeChoseView.swift
//  Nierika
//
//  Created by Jose Manuel Chairez Macias on 19/08/18.
//  Copyright © 2018 Juan Carlos. All rights reserved.
//

import UIKit
protocol LanguajeProtocol{
    func setLanguageSelected(language: String)
}

class LenguajeChoseView: UIViewController {

    @IBOutlet weak var windowView: UIView!
    

    var languageProtocol:LanguajeProtocol?	
    var language: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func changeLanguage(sender: AnyObject) {
        guard let button = sender as? UIButton else {
            return
        }
        print(button.tag)
        switch button.tag {
            case 0:
                languageProtocol?.setLanguageSelected(language: "wixarika")
            case 1:
                languageProtocol?.setLanguageSelected(language: "español")
            case 2:
                languageProtocol?.setLanguageSelected(language: "english")
            case 3:
                languageProtocol?.setLanguageSelected(language: "french")
           default:
            print("Unknown language")
            return
        }
        dismiss(animated: true) {
            
        }
    }
}
